CC = g++
CFLAGS = -Wall -pthread
CFLAGS_DEBUG = $(CFLAGS) -g -O0 -v -da -Q

default: alloc prov_rep

alloc: alloc.o
	$(CC) $(CFLAGS) -o alloc alloc.o

alloc.o: alloc.cpp
	$(CC) $(CFLAGS) -c alloc.cpp

prov_rep: prov-rep.o
	$(CC) $(CFLAGS) -o prov-rep prov-rep.o

prov_rep.o: prov-rep.cpp
	$(CC) $(CFLAGS) -c prov-rep.cpp

clean:
	$(RM) *.o *~ *.cpp.* alloc prov-rep