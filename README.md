The provider - Reporter code lives in prov-rep.cpp. The allocator code lives in alloc.cpp. They can only handle resources with one digit, 
but it can be any character besides a space.

When you quit the provider, it sends a sigterm signal to the reporter, so it will also quit.

I'm using make, so all you have to do to compile the code is type "make", and then it will give you the compiled "alloc" and "prov-rep".