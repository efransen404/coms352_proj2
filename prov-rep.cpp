/******************************************************************************
 * Created by Eric Fransen on 4-14-2019                                       *
 ******************************************************************************/

// library includes
#include <string>
#include <string.h>
#include <cstdio>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/fcntl.h>
#include <unistd.h>
#include <cstdio>
#include <iostream>
#include <stdlib.h>
#include <cstdlib>
#include <sstream>
#include <signal.h>

// function declarations
int provider();
int reporter();
int exit();
void print_help_message();
bool provide(char resource, int value);

// global variables
#define FILEPATH "res.txt"
// filepath for the resource file
std::string filepath = FILEPATH;
// resource file itself
FILE *resource_file;
// file descriptor
int fd;
// memory mapping of resource file
char * region;
// size of the file
int fsize;
// id for the semaphore
int semID = -1;
// sembuf to lock the semaphore
struct sembuf sem_lock = { 0, -1, 0 };
// sembuf to unlock the semaphore
struct sembuf sem_unlock = { 0, 1, 0};
// sembuf to lock the semaphore with no blocking
struct sembuf sem_lock_noWait = {0, -1, IPC_NOWAIT};
// sembuf to check for uninitialized value (0) with no blocking
struct sembuf sem_check_zero = {0, 0, IPC_NOWAIT};
// The child PID
pid_t child_pid = -1;


/******************************************************************************
 * The main method does the set up for the provider - reporter processes      *
 * It opens the file, gets the semaphore, and then creates a child process    *
 * After the fork, control is passed to provider() and reporter().            *
 * This process will simply return whatever those processes return.           *
 ******************************************************************************/
int main(void) {
    // open the file
    resource_file = fopen(filepath.c_str(), "r+");
    // get the file descriptor
    fd = fileno(resource_file);
    // use the file descriptor as a key for a semaphore
    key_t fileKey = fd;
    // We're only using one semaphore
    int nsems = 1;
    int semflg = O_RDWR | S_IRUSR | S_IWUSR;
    semID = semget(fileKey, 1, semflg);
    if(semID == -1) {
        // semget failed, possibly because sem doesn't exist
        semID = semget(fileKey, 1, semflg | IPC_CREAT);
        if(semID == -1) {
            // semget still failed, so we're erroring out
            perror("semget failed!\n");
            exit();
            return -1;
        } else {
            // we got a new semaphore, now we need to initialize it
            if(-1 == semctl(semID, 0, SETVAL, 1)) {
                // something went wrong, error out
                perror("semctl failed!\n");
                return -1;
            }
        }
    }
    // get the file stats
    struct stat sb;
    fstat(fd, &sb);
    // get the size
    fsize = sb.st_size;
    // acquire the lock
    semop(semID, &sem_lock, 1);
    // map res.txt to memory
    region = (char*) mmap(
        NULL,
        fsize,
        PROT_READ | PROT_WRITE,
        MAP_SHARED,
        fd,
        0
    );
    // release the lock
    semop(semID, &sem_unlock, 1);
    child_pid = fork();
    if(child_pid == 0) {
        //child process
        return(reporter());
    } else {
        return(provider());
    }
}

/******************************************************************************
 * A helper method to handle clean up stuff, like closing files.              *
 ******************************************************************************/
int exit() {
    std::cout << "Closing...\n";
    std::cout << "Closing resource file...\n";
    fclose(resource_file);
    std::cout << "Sending SIGTERM to child process...\n";
    kill(child_pid, SIGTERM);
}

/******************************************************************************
 * A helper method for provider functionality.                                *
 * It loops asking if resources need to be added. If yes, it takes the user   *
 * input and adds it to the memory region, and then calls msync               *
 ******************************************************************************/
int provider() {
    // Loop forever
    while(true) {
        print_help_message();
        // get the entire input as a string
        std::string input;
        getline(std::cin, input);
        // pass the string into a stringstream
        std::stringstream instream(input);
        std::string arg;
        // take the first word as an argument
        instream >> arg;
        // if the argument is q, then we break the loop and return
        if(arg.compare("q") == 0) {
            return exit();
        }
        // reset the stringstream
        instream.str(input);
        int value;
        char resource;
        // parse the string into a char and an int if we can
        if(instream >> resource) {
            if(instream >> value) {
                // If the value is less than zero, make sure the user wants to provide a negative amount
                if(value < 0) {
                    std::cout << "You're trying to provide a negative amount of a resource. Are you sure?\n";
                    std::cout << "Type \'\' to continue provision, or \'n\' to cancel\n";
                    // loop until we get a valid answer
                    bool valid = false;
                    while(!valid) {
                        getline(std::cin, input);
                        instream.str(input);
                        // the user wants to provide the negative amount, so we'll do it
                        if(input[0] == 'y') {
                            valid = true;
                            provide(resource, value);
                        } else if(input[0] == 'n') { // the user changed their mind, so we'll back out of this loop
                            valid = true;
                        }
                    }
                } else {
                    // we'll just provide anything greater than zero
                    provide(resource, value);
                }
            }
        }

    }
    // we'll never reach this, but the compiler complained about it
    return 0;
}

/******************************************************************************
 * A helper method to parse the output from mincore                           *
 * If the char from mincore has a 1, we'll put a 1 in the string.             *
 * If the char from minctre doesn't have a 1, we'll put a 0 in the string.    *
 * In essense, we get a 1 for every page in memory, and a 0 for any page out  *
 * of memory.                                                                 *
 * Inputs:                                                                    *
 *   vec: The unsigned char array from mincore                                *
 *   length: The size of vec                                                  *
 * Outputs:                                                                   *
 *   A 1 for every page in memory, and a 0 for every page out of memory.      *
 ******************************************************************************/
std::string parse_mincore(unsigned char* vec, int length) {
    std::string out = "";
    for(int i = 0; i < length; i++) {
        if(vec[i] & 00000001) {
            out.append("1");
        } else {
            out.append("0");
        }
    }
    return out;
}

/******************************************************************************
 * A helper method for reporter functionality.                                *
 * Every 10 seconds, it will report                                           *
 *   The page size of the system                                              *
 *   The current state of resources                                           *
 *   The current status of pages in the memory region                         *
 ******************************************************************************/
int reporter() {
    int pagesize = getpagesize();
    int vec_length = (fsize + pagesize - 1) / pagesize;
    unsigned char vec[vec_length];
    while(true) {
        printf("\n--------------------------------------------------------------------------------\n");
        printf("Page size: %d\n", getpagesize());
        semop(semID, &sem_lock, 1);
        printf("%s\n", region);
        semop(semID, &sem_unlock, 1);
        mincore(region, fsize, vec);
        std::string status = parse_mincore(vec, vec_length);
        printf("Page status:%s\n", status.c_str());
        printf("--------------------------------------------------------------------------------\n");
        sleep(10);
    }
    return 0;
}

/******************************************************************************
 * A helper method to print some help messages.                               *
 * Inputs: None                                                               *
 * Outputs: None, but some text is output to the console.                     *
 ******************************************************************************/
void print_help_message() {
    std::cout << "You can quit by typing \"q\".\n";
    std::cout << "Enter the resource and amount you'd like to provide\n";
    std::cout << "Use the format $R $A\n";
}

/******************************************************************************
 * A function to attempt to privide a resource and value.                     *
 * Inputs:                                                                    *
 *  _resource: The resource we want to try to provide.                        *
 *  _value: The amount we want to try to provide.                             *
 * Return:                                                                    *
 *  Returns true if successful, false otherwise.                              *
 * The function iterates over the entire file, finding resource value pairs,  *
 *   until it either finds the desired resource, or the end of the file is    *
 *   reached. If the end of the file is reached, a message is shown stating   *
 *   the resource was not found.                                              *
 * When the desired resource is found, the value is checked to see if the     *
 *   provision is possible. If the provision can be done, the value is        *
 *   changed, a message is shown, and the function returns true. If the       *
 *   provision cannot be done, a different message is shown and the function  *
 *   returns false.                                                           *
 ******************************************************************************/
bool provide(char _resource, int _value) {
    // index values for the resource and value
    int r_index, v_index;
    r_index = 0;
    v_index = 0;
    semop(semID, &sem_lock, 1);
    while(true) {
        // First we have to find a char for the resource
        while(region[r_index] == ' ' || region[r_index] == '\n') {
            // we move both indexes together here
            r_index++;
            v_index++;
            // if the index went out of bounds, return
            if(r_index == fsize) {
                std::cout << "Resource not found!\n";
                semop(semID, &sem_unlock, 1);
                return false;
            }
        }
        // we found the index for the next resource, now we have to find the value after it
        v_index++;
        while(!isdigit(region[v_index])) {
            // Now we're just moving the value index forward
            v_index++;
            // If the value index goes out of bounds, return
            if(v_index == fsize) {
                std::cout << "Value index went out of bounds!\n";
                semop(semID, &sem_unlock, 1);
                return false;
            }
        }
        // Now we have the indexes for the resource and the value
        int resource, value;
        resource = region[r_index];
        value = region[v_index] - '0';
        // check if the resource is the one we're interested in
        if(resource == _resource) {
            // check if the new value will be valid
            int new_value = value + _value;
            if(new_value < 0) {
                // We were adding a negative value, and it made the new value go below zero. We won't allocate it.
                std::cout << "Providing new value would go below zero. Aborting provision!\n";
                semop(semID, &sem_unlock, 1);
                return false;
            } else if(new_value > 9) {
                // We're adding a value, and it went above 9. We won't allocate it
                std::cout << "Providing new value would go above nine! Aborting provision!\n";
                semop(semID, &sem_unlock, 1);
                return false;
            } else {
                // This is a valid allocation, so we assign the new value
                region[v_index] = '0' + new_value;
                // sync the file with memory
                msync(region, 0, MS_SYNC);
                std::cout << "Provided successfully!\n";
                semop(semID, &sem_unlock, 1);
                return true;
            }
        } else {
            // This pair was incorrect, so we have to move the indexes past the current value
            r_index = v_index + 1;
            v_index++;
            // Check if we went out of bounds
            if(r_index == fsize) {
                std::cout << "Resource not found!\n";
                semop(semID, &sem_unlock, 1);
                return false;
            }
        }
    }
}