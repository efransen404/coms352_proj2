/******************************************************************************
 * Created by Eric Fransen on 4-14-2019                                       *
 * Contains all the code for my allocator program.                            *
 ******************************************************************************/

// library includes
#include <cstdio>
#include <iostream>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <unistd.h>
#include <errno.h>
#include <sstream>
#include <fcntl.h>

// defines the default filepath
#define FILEPATH "res.txt"

// function declarations
void printResourceStatus();
void printHelpMessage();
int exit();
bool allocate(char resource, int amount);

// global variables
// filepath for the resource file
std::string filepath = FILEPATH;
// resource file
FILE *resource_file;
// file descriptor for resource file
int fd;
// memory mapping of resource file
char * region;
// size of the file
int fsize;
// id for the semephore
int semID = -1;
// sembuf to lock the semaphore
struct sembuf sem_lock = { 0, -1, 0 };
// sembuf to unlock the semaphore
struct sembuf sem_unlock = {0, 1, 0};



/******************************************************************************
 * Main function for the allocator. Takes no inputs, and returns 0 when closed*
 * normally, and 1 if the memory mapping fails.                               *
 * It first opens the file, creates a memory mapping, and then enters a loop  *
 * In the loop, the function will display the status of the file, a prompt for*
 * entry, and then wait for input. When input is received, it checks if it is *
 * the argument to quit, in which case it closes the process, or it tries to  *
 * allocate the requested values.                                             *
 ******************************************************************************/
int main(void) {
    // get the size of the file
    struct stat sb;
    // open the file
    resource_file = fopen(filepath.c_str(), "r+");
    // get the file descriptor
    fd = fileno(resource_file);
    // use the file descriptor as a key for a semaphore
    key_t fileKey = fd;
    // We're using one semaphore
    int nsums = 1;
    // We want to create a semaphore, but don't care if it exists already
    int semflg = O_RDWR | S_IRUSR | S_IWUSR;
    // Get the semaphore
    semID = semget(fileKey, nsums, semflg);
    if(semID == -1) {
        // semget failed, possibly because sem doesn't exist yet
        semID = semget(fileKey, 1, semflg | IPC_CREAT);
        if(semID == -1) {
            // we tried to create the semaphore, and semget still failed
            perror("semget failed!\n");
            exit();
            return -1;
        } else {
            if(-1 == semctl(semID, 0, SETVAL, 1)) {
                perror("semctl failed!\n");
                return -1;
            }
        }
    }

    // get the file stats
    fstat(fd, &sb);
    // get the size
    fsize = sb.st_size;
    // aqcuire the lock
    semop(semID, &sem_lock, 1);
    // map res.txt to memory
    region = (char*) mmap(
        NULL,
        fsize,
        PROT_READ | PROT_WRITE,
        MAP_SHARED,
        fd,
        0
    );
    // release the lock
    semop(semID, &sem_unlock, 1);

    // Display an error meessage if the mapping fails
    if (region == MAP_FAILED) {
        perror("mmap failed");
        return 1;
    }
    while(true) {
        printResourceStatus();
        printHelpMessage();
        std::string input;
        // Get the input line
        getline(std::cin, input);
        // create a stringstream over the line of input
        std::stringstream instream(input);
        std::string arg;
        instream >> arg;
        // if the argument is q, we quit
        if(arg.compare("q") == 0) {
            return exit();
        }
        // reset the input stream
        instream.str(input);
        char resource;
        int value;
        // check if we got a char and an int
        if(instream >> resource) {
            if(instream >> value) {
                if(value < 0) {
                    std::cout << "You're trying to allocate a negative amount of the resource. Are you sure?\n";
                    std::cout << "Type \'y\' to continue allocation, or \'n\' to cancel\n";
                    bool valid = false;
                    while(!valid) {
                        getline(std::cin, input);
                        instream.str(input);
                        if(input[0] == 'y') {
                            valid = true;
                            allocate(resource, value);
                        } else if (input[0] == 'n') {
                            valid = true;
                        }
                    }
                } else {
                    allocate(resource, value);
                }
            }
        }
    }
}

/******************************************************************************
 * A helper method which takes no inputs and returns nothing. It just prints  *
 * the current status of the memory mapped file.                              *
 ******************************************************************************/
void printResourceStatus() {
    std::cout << "Current resource values:\n" << region << "\n";
}

/******************************************************************************
 * Another helper method which takes no input and returns nothing. It just    *
 * prints a prompt for entry.                                                 *
 ******************************************************************************/
void printHelpMessage() {
    std::cout << "Enter \"q\" to quit, or the resource and amount to allocate\n";
}

/******************************************************************************
 * Another helper function which takes no input. It returns a status code, 0  *
 * for exiting normally. It performs any remainding tasks when closing.       *
 ******************************************************************************/
int exit() {
    std::cout << "Exiting...\n";
    std::cout << "Closing resource file...\n";
    fclose(resource_file);
    return 0;
}

/******************************************************************************
 * A function to attempt to allocate a resource and value.                    *
 * Inputs:                                                                    *
 *  req_resource: The resource we want to try to allocate.                    *
 *  req_value: The amount we want to try to allocate.                         *
 * Return:                                                                    *
 *  Returns true if successful, false otherwise.                              *
 * The function iterates over the entire file, finding resource value pairs,  *
 *   until it either finds the desired resource, or the end of the file is    *
 *   reached. If the end of the file is reached, a message is shown stating   *
 *   the resource was not found.                                              *
 * When the desired resource is found, the value is checked to see if the     *
 *   allocation is possible. If the allocation can be done, the value is      *
 *   changed, a message is shown, and the function returns true. If the       *
 *   allocation cannot be done, a different message is shown and the function *
 *   returns false.                                                           *
 ******************************************************************************/
bool allocate(char req_resource, int req_value) {
    std::cout << "Attempting to allocate " << req_value << " from resource " << req_resource << "\n";
    // values for the index of the resource and value in the region
    int r_index, v_index;
    r_index = 0;
    v_index = 0;
    // flag to mark when we're done
    bool done = false;
    // We wait on the semaphore
    semop(semID, &sem_lock, 1);
    // while we're not done
    while(!done) {
        // while r_index is on a whitespace
        while(region[r_index] == ' ' || region[r_index] == '\n') {
            // increment r and v
            r_index++;
            v_index++;
            if(r_index == fsize) {
                std::cout << "Resource not found!\n";
                semop(semID, &sem_unlock, 1);
                return false;
            }
        }
        // r is on a digit, now we move v past it
        v_index++;
        // while v isn't a digit
        while(!isdigit(region[v_index])) {
            if(v_index == fsize) {
                std::cout << "Value index out of bounds!\n";
                semop(semID, &sem_unlock, 1);
                return false;
            }
            v_index++;
        }
        // now we know r is on valid char, and v is on a digit
        // because of the layout of the file, we know r is a resource, and v is a value
        int resource, value;
        resource = region[r_index];
        value = region[v_index] - '0';
        // check if the region is the one we're interested in
        if(resource == req_resource) {
            // check if we have enough resources to allocate
            if(value >= req_value) {
                // calculate the new value
                int new_value = value - req_value;
                if(new_value > 9) {
                    std::cout << "Resource value overflow! Setting value to 9.\n";
                    new_value = 9;
                }
                // set the new value
                region[v_index] = '0' + new_value;
                // sync the file with memory
                msync(region, 0, MS_SYNC);
                std::cout << "Allocated successfully!\n";
                semop(semID, &sem_unlock, 1);
                return true;
            } else {
                std::cout << "Not enough of resource " << resource << " to allocate!\n";
                semop(semID, &sem_unlock, 1);
                return false;
            }
        } else {
            // This pair is incorrect, now we have to move r and v to the next pair
            r_index = v_index + 1;
            v_index = r_index;
            if(r_index == fsize) {
                std::cout << "Resource not found!\n";
                semop(semID, &sem_unlock, 1);
                return false;
            }
        }
    }

    semop(semID, &sem_unlock, 1);
    // We should never reach here, but compiler was complaining
    return false;
}